from datetime import datetime
import locale

locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')

temperature = int(input('Введите температуру\n'))
day = datetime.now().date().strftime('%d')
month = datetime.now().date().strftime('%B')
if month == 'Март' or month == 'Августа':
    month = month[:-1] + 'а'
else:
    month = month[:-1] + 'я'
if temperature < 0:
    print(f'Сегодня {day} {month}. На улице {temperature}.\nХолодно, лучше остаться дома')
else:
    print(f'Сегодня {day} {month}. На улице {temperature}.')
