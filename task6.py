value = int(input('Давай проверим число\n'))
if value ==1:
    print('Это единичное число')
elif value == 2:
    print('Это простое число')
elif value > 2:
    for i in range(2, int(value ** 0.5) + 1):
        if value % i == 0:
            print("Это составное число")
            break
    else:
        print('Это простое число')
